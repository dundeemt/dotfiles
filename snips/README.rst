******************
Snippets
******************

Most snippets come from: https://github.com/honza/vim-snippets

As I personalize them, I move them under: ** My Snippets **

License: MIT

