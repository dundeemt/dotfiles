#/usr/bin/env bash
# update dot files links in my home directory.
# add any dotfiles you want to keep sync'd to a rename listed
# in the dotfiles array
#
dotfiles=( "bash_aliases" "pylintrc" "tmux.conf" "vimrc" "snips" )
for dotfile in "${dotfiles[@]}"
do
	if [ -e "$HOME/.$dotfile" ];
	then
		echo "$HOME/.$dotfile exists - testing."
		if [ -L "$HOME/.$dotfile" ]
		then
			echo "$HOME/.$dotfile is a link - remove"
			rm "$HOME/.$dotfile"
		else
			echo "$HOME/.$dotfile is real - quit and fix"
			exit 1
		fi
	else
		echo "$HOME/.$dotfile does not exist."
		if [ -L "$HOME/.$dotfile" ]
		then
			echo "$HOME/.$dotfile is a broken symlink - remove"
			rm "$HOME/.$dotfile"
		fi
	
	fi
	echo "create link $PWD/$dotfile <- $HOME/.$dotfile"
	ln -s "$PWD/$dotfile" "$HOME/.$dotfile"
done

