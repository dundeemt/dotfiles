set nocompatible              " required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

" Add all your plugins here (note older versions of Vundle used Bundle instead of Plugin)
Plugin 'tmhedberg/SimpylFold'
Plugin 'vim-scripts/indentpython.vim'
Plugin 'Valloric/YouCompleteMe'
Plugin 'jnurmine/Zenburn'
Plugin 'altercation/vim-colors-solarized'
Plugin 'sickill/vim-monokai'
Plugin 'tomasr/molokai'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'scrooloose/nerdtree'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'scrooloose/syntastic'
Plugin 'elzr/vim-json'
Plugin 'janko-m/vim-test'
Bundle 'roman/golden-ratio'
Bundle 'tpope/vim-dispatch'
Plugin 'tpope/vim-obsession'
Plugin 'tomtom/tcomment_vim'
Plugin 'sjl/gundo.vim'
Plugin 'sirver/ultisnips'
Plugin 'rking/ag.vim'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
"
" colorize
colorscheme monokai
"
" make sure syntax highlighting is enabled
syntax on
"
" show me
set showcmd
"
" YouCompleteMe settings
let g:ycm_autoclose_preview_window_after_completion=1
map <leader>g  :YcmCompleter GoToDefinitionElseDeclaration<CR>
"
" don't let vim-test clear the screen
let g:test#preserve_screen = 1
nmap <silent> <leader>t :TestNearest -v<CR>
nmap <silent> <leader>T :TestFile -vx<CR>
nmap <silent> <leader>a :TestSuite<CR>
nmap <silent> <leader>l :TestLast<CR>
nmap <silent> <leader>j :TestVisit<CR>
"
" how splitting should occur
set splitbelow
set splitright
" move between windows Ctrl-[j|k|l|h]
"split navigations
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
"
" map bNext/bPrev to <Alt>-right/left arrow
"
:map <Right> :hide bn<CR>
:map <Left>  :hide bp<CR>
"
"" Ctrl-S (Smart Save)
" If the current buffer has never been saved, it will have no name,
" call the file browser to save it, otherwise just save it.
" http://unix.stackexchange.com/questions/59770/saving-with-ctrl-s-in-vim
nnoremap <silent> <C-S> :if expand("%") == ""<CR>browse confirm w<CR>else<CR>confirm w<CR>endif<CR>
" if in INSERT mode, goto cmd line, issue write then return to insert mode
inoremap <C-S> <Esc>:w<cr>
"
" Enable folding
let g:SimpylFold_docstring_preview = 0
:augroup simplyfold
:	autocmd!
:	autocmd BufWinEnter *.py setlocal foldexpr=SimpylFold(v:lnum) foldmethod=expr
:	autocmd BufWinLeave *.py setlocal foldexpr< foldmethod<
:augroup END 
let php_folding=1

" Enable folding with the spacebar
nnoremap <space> za
set foldlevelstart=3
set foldlevel=3
"
" setup search
"
set ignorecase
set smartcase
set incsearch
set hlsearch
"
" identification
" 
:augroup identification
:	autocmd!
:	autocmd BufRead,BufNewFile *.json set filetype=json
:	autocmd BufNewFile,BufRead *.html set filetype=htmldjango
:augroup END
"
" indentation
"
autocmd BufNewFile,BufRead *.py
    \ set tabstop=4     |
    \ set softtabstop=4 |
    \ set shiftwidth=4  |
    \ set textwidth=79  |
    \ set expandtab     |
    \ set autoindent    |
    \ set fileformat=unix |

autocmd BufNewFile,BufRead *.js, *.html, *.css
    \ set tabstop=2     |
    \ set softtabstop=2 |
    \ set shiftwidth=2  |

:augroup indentation
:	autocmd!
:       autocmd FileType json setlocal ts=4 sts=4 sw=4 expandtab fileformat=unix textwidth=0 wrapmargin=0
:       autocmd FileType yaml setlocal ts=4 sts=4 sw=4 expandtab fileformat=unix textwidth=0 wrapmargin=0
:       autocmd FileType rst  setlocal ts=4 sts=4 sw=4 expandtab textwidth=79 fileformat=unix
:augroup END
"
" flag unnecessary whitespace
highlight BadWhitespace ctermbg=red guibg=darkred
autocmd BufRead,BufNewFile *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/
" remove trailing white space on save - python
autocmd FileType python autocmd BufWritePre <buffer> :%s/\s\+$//e

" utf-8 support
set encoding=utf-8
"
" map nerdtree to toggle on Ctrl-N or F3
map <C-n> :NERDTreeToggle<CR>
map <F3>  :NERDTreeToggle<CR>
" close nerdtree on open
let NERDTreeQuitOnOpen=1
let NERDTreeIgnore = ['\.pyc$']
"
" turn on hybrid relative line numbering
set relativenumber
set number
"
" always show airline
set laststatus=2
" use powerline fonts
let g:airline_powerline_fonts=1
" Enable the list of buffers
let g:airline#extensions#tabline#enabled = 1
" Show just the filename
let g:airline#extensions#tabline#fnamemod = ':t'
" prepend a $ to the section if Obsession is active
let g:airline_section_z = airline#section#create(['%{ObsessionStatus(''$'', '''')}', 'windowswap', '%3p%% ', 'linenr', ':%3v '])
"
"python with virtualenv support
if has("python3")
  command! -nargs=1 Py py3 <args>
else
  command! -nargs=1 Py py <args>
endif
Py << EOF
import os
import sys
if 'VIRTUAL_ENV' in os.environ:
  project_base_dir = os.environ['VIRTUAL_ENV']
  activate_this = os.path.join(project_base_dir, 'bin/activate_this.py')
  execfile(activate_this, dict(__file__=activate_this))
EOF

" syntastic settings
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
" let g:syntastic_check_on_wq = 0
"let g:syntastic_python_pylint_args = "--load-plugins django_pylint"
let g:syntastic_python_pylint_post_args = '--msg-template="{path}:{line}:{column}:{C}: [{symbol} {msg_id}] {msg}"'

" used by ctrlp to ignore
set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*.jpg,*.pyc     " Linux/MacOSX
"
" Paste Toggle
nnoremap <F2> :set invpaste paste?<CR>
set pastetoggle=<F2>
set showmode
"
" spell check toggle
map <F4> :setlocal spell! spelllang=en_us<CR>
"
" gundo Toggle
nnoremap <F5> :GundoToggle<CR>
"
" set autoread and autowrite
set autoread
set autowrite
"
" Check for file modifications automatically
" (current buffer only).
" Use :NoAutoChecktime to disable it (uses b:autochecktime)
"
fun! MyAutoCheckTime()
  " only check timestamp for normal files
  if &buftype != '' | return | endif
  if ! exists('b:autochecktime') || b:autochecktime
    checktime %
    let b:autochecktime = 1
  endif
endfun
augroup MyAutoChecktime
  au!
  au FocusGained,BufEnter,CursorHold * call MyAutoCheckTime()
augroup END
command! NoAutoChecktime let b:autochecktime=0
"
" UltiSnip Config
let g:UltiSnipsExpandTrigger="<c-j>"
let g:UltiSnipsSnippetsDir="~/.snips"
let g:UltiSnipsSnippetDirectories=[$HOME.'/.snips']
let g:ultisnips_python_style="google"
" vim:set et sw=2:
