my dotfiles
===========

"These are my dotfiles.  There are many like them, but these are mine."


vim setup
---------
Get vim + tmux installed and config'd

::

        sudo apt-get install vim cmake git tmux build-essential python3-dev mercurial
        git clone https://github.com/gmarik/Vundle.vim.git ~/.vim/bundle/Vundle.vim
        ./updot.sh
        vim +PluginInstall
        :qa
        cd ~/.vim/bundle/YouCompleteMe/
        python3 install.py
        cd ~


install tmx
-----------
see https://bitbucket.org/dundeemt/tmx

::

        wget https://bitbucket.org/dundeemt/tmx/raw/tip/tmx.py


Map Caps Lock -> Escape
-----------------------
Less of stretch to get escape this way

        sudo vi /etc/default/keyboard
                XKBOPTIONS="caps:escape"
        sudo dpkg-reconfigure keyboard-configuration


Keyboard Settings
-------------------
Settings | Keyboard | Behavior

Type Settings:

::

        Repeat Delay: 400 (default: 500)
        Repeat Speed: 40  (default: 20)

*NOTE* output a map of keys

::

        :redir! > vim_keys.txt
        :silent verbose map
        :redir END

also, :imap and :vmap


JSON linting
------------

::

        sudo npm -g install jsonlint


simple .bash_profile
--------------------

::

    # don't put duplicate lines or lines starting with space in the history.
    # See bash(1) for more options
    HISTCONTROL=ignoreboth

    # append to the history file, don't overwrite it
    shopt -s histappend

    # for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
    HISTSIZE=10000
    HISTFILESIZE=10000

    # check the window size after each command and, if necessary,
    # update the values of LINES and COLUMNS.
    shopt -s checkwinsize

    # If set, the pattern "**" used in a pathname expansion context will
    # match all files and zero or more directories and subdirectories.
    #shopt -s globstar

    # Alias definitions.
    # You may want to put all your additions into a separate file like
    # ~/.bash_aliases, instead of adding them here directly.
    # See /usr/share/doc/bash-doc/examples in the bash-doc package.

    if [ -f ~/.bash_aliases ]; then
        . ~/.bash_aliases
    fi

