# personalizations go here
#
alias tmux='tmux -2'
alias tma='tmux -2 attach'
alias tmx='python ~/sync/common/tmx/tmx.py'

alias less='less -R'
alias df='df -ah'

# Give me back Ctrl-s a/k/a C-s  Tres Cool, n'est pas?
# No ttyctl, so we need to save and then restore terminal settings
# http://unix.stackexchange.com/questions/59770/saving-with-ctrl-s-in-vim
vim()
{
	local ret STTYOPTS="$(stty -g)"
        stty -ixon
        command vim "$@"
        ret=$?
        stty "$STTYOPTS"
        return "$ret"
}
# load bash completions for mercurial if available
if [ -f /usr/local/share/mercurial/contrib/bash_completion ]; then
    . /usr/local/share/mercurial/contrib/bash_completion
fi

